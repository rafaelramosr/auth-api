from fastapi import APIRouter
from ..api.code.routes import code_routes

router = APIRouter()
router.include_router(code_routes)
