from .schemas import Code
from app.helpers import gen_code
from app.db import core


def code_generate() -> Code:
    code = gen_code.get_code()
    isCode = core.get('codes', 'code', code)

    if isCode == None:
        core.insert('codes', { 'code': code })
        return { 'code': code  }

    return code_generate()
