from fastapi import APIRouter, Depends

from .schemas import Code
from .controllers import cargos, code_generate, permissions


code_routes = APIRouter(
    prefix="/code",
    tags=["code"],
    responses={404: {"description": "Not found"}},
)


@code_routes.post("/")
def get_code(user_data: Code = Depends(code_generate)):
    return user_data


@code_routes.post("/session")
def session(session = Depends(permissions)):
    return session


@code_routes.get("/cargos")
def get_cargos(session = Depends(cargos)):
    return session
