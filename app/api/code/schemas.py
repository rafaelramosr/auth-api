from pydantic import BaseModel
from typing import Optional


class Code(BaseModel):
    code: int
    toke: Optional[str]
