from random import randrange

def get_code() -> int:
    return randrange(1000, 9999)
