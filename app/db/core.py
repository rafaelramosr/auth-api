from tinydb import Query, TinyDB, where
from typing import Any


def get_db() -> TinyDB:
    db = TinyDB('db.json')
    return db


def model_factory(model_name: str):
    return get_db().table(model_name)


def get(model_name: str, key: str, value: str | int) -> Any | None:
    return model_factory(model_name).get(where(key) == value)


def insert(model_name: str, data: Any) -> int:
    return model_factory(model_name).insert(data)


def update(model_name: str, data: Any, key: str, value: str | int) -> int:
    result = model_factory(model_name).update(data, Query()[key] == value)
    return result[0]
