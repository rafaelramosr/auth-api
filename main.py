from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError
from fastapi.exception_handlers import (
    http_exception_handler,
    request_validation_exception_handler,
)
from starlette.middleware import Middleware
from starlette.exceptions import HTTPException as StarletteHTTPException

from app.routers.routers import router

middleware = [
    Middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
]

app = FastAPI(middleware=middleware)

app.include_router(router)


@app.exception_handler(StarletteHTTPException)
async def custom_http_exception_handler(request: Request, exc: StarletteHTTPException):
    response: JSONResponse = await http_exception_handler(request, exc)
    print(str(request.url), repr(exc), response.status_code)
    return response


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    response: JSONResponse = await request_validation_exception_handler(request, exc)
    print(str(request.url), repr(exc), response.status_code)
    return response


@app.get("/")
async def root():
    return {"message": "Welcome to AUTH API!"}
