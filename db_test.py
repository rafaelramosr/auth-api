from tinydb import TinyDB, Query, where
db = TinyDB('db.json')

ModelQuery = Query()
model = db.table('codes')

# create -> id
r = model.insert({'code': 1290})
print('create: ', r)

# read -> array model
r = model.all()
print('read: ', r)

# update -> array id
r = model.update({'jwt':'rrrr'}, ModelQuery.code==1290)
print('update: ', r)

# read one -> model
r = model.get(where('code') == 1290)
print('read one: ', r)

# delete


